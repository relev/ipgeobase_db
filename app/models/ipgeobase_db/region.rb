module IpgeobaseDb
  class Region < ActiveRecord::Base
    belongs_to :district
    has_many :cities
    has_many :records
  end
end
