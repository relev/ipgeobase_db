module IpgeobaseDb
  class District < ActiveRecord::Base
    has_many :regions
    has_many :records
  end
end
