module IpgeobaseDb
  class City < ActiveRecord::Base
    belongs_to :region
    belongs_to :district
    has_many :records
  end
end
