module IpgeobaseDb
  class Record < ActiveRecord::Base
    belongs_to :city
    belongs_to :region
    belongs_to :district

    scope :with_ip, ->(ip) { where("inet_aton('#{ip}') BETWEEN low AND high")[0] }
  end
end
