module IpgeobaseDb
  class InfoService

    def self.city(ip)
      r = record(ip)
      r && r.city
    end

    def self.record(ip)
      r = Record.with_ip(ip)
      r.is_a?(Record) ? r : nil
    end

  end
end
