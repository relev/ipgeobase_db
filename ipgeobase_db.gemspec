$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "ipgeobase_db/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "ipgeobase_db"
  s.version     = IpgeobaseDb::VERSION
  s.authors     = ["Rechnik Lev"]
  s.email       = ["<censored>"]
  s.homepage    = "https://bitbucket.org/headmadepro/ipgeobase_db"
  s.summary     = "Stores ipgeobase in postgres."
  s.description = "Stores ipgeobase in postgres (http://ipgeobase.ru). And serves."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc", "README.md"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency 'rails', '~> 4.1.1'
  s.add_dependency 'pg'

  s.add_development_dependency "spring"
end
