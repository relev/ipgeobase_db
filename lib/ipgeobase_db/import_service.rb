module IpgeobaseDb
  class ImportService

    def self.download
      cmd = "mkdir -p '%s' && wget -q -O %s '%s'" % [tmp_dir,tmp_file_archive,'http://ipgeobase.ru/files/db/Main/geo_files.tar.gz']
      puts ' - ' + cmd
      system(cmd)
    end

    # Загружает данные во временные new-таблицы, потом
    # транзакционно переименовывает текущие в old, а new - в нормальные имена
    def self.load
      cmd = "cd '%s' && tar -zxvf '%s'" % [tmp_dir,tmp_file_archive]
      puts ' - ' + cmd
      system(cmd)

      klasses = [Record, City, Region, District]

      init_table_names(klasses)
      set_tables_new(klasses)
      create_tables_new(klasses)

      query_base = "INSERT INTO #{@table_new[City]} (created_at,id,name,region_name,district_name,lat,lon,country_code,district_id,region_id) VALUES "
      values = []

      require 'csv'

      regions = {}
      districts = {}
      CSV.foreach(tmp_file_cities, {col_sep: "\t", encoding: 'cp1251', quote_char: "\t"}) do |row|
        district_name = row[3]
        country_code = district_name.index('Украина').present? ? 'UA' : 'RU'

        district = (districts[district_name] ||= District.create(name: district_name, country_code: country_code))

        region_name = row[2]
        region = (regions[region_name] ||= Region.create(name: region_name, district_name: district_name, district_id: district.id, country_code: country_code))

        values << "(NOW(),%d,'%s','%s','%s',%s,%s,'%s',%d,%d)" % (row + [country_code,district.id,region.id])
      end

      query = query_base + values.join(',')
      City.connection.execute(query)

      query_base = "INSERT INTO #{@table_new[Record]} (created_at,low,high,country_code,city_id,region_id,district_id) VALUES "
      values = []
      delta = 1 << 31
      cities = {}
      CSV.foreach(tmp_file_cidr_optim, {col_sep: "\t", encoding: 'cp1251', quote_char: "\t"}) do |row|
        city_id=row[4]
        if city_id == '-'
          city_id = region_id = district_id = 'NULL'
        else
          city = (cities[city_id] ||= City.find(city_id))
          region_id = city.region_id
          district_id = city.district_id
        end

        rec = [row[0].to_i-delta,row[1].to_i-delta,row[3],city_id,region_id,district_id]

        values << "(NOW(),%d,%d,'%s',%s,%s,%s)" % rec
      end

      query = query_base + values.join(',')
      City.connection.execute(query)

      set_tables_original(klasses)
      rotate_tables(klasses)

    end

    private
    def self.tmp_base() '/tmp'; end
    def self.tmp_dir() [tmp_base,'ipgeobase_db'].join('/'); end
    def self.tmp_file(fname) [tmp_base,'ipgeobase_db',fname].join('/'); end

    def self.tmp_file_archive() tmp_file('geo_files.tar.gz'); end
    def self.tmp_file_cities() tmp_file('cities.txt'); end
    def self.tmp_file_cidr_optim() tmp_file('cidr_optim.txt'); end

    def self.table_name_old(klass) "__old__#{klass.table_name}"; end
    def self.table_name_new(klass) "__new__#{klass.table_name}"; end

    def self.seq_name(klass) "#{klass.table_name}_id_seq"; end

    def self.init_table_names(klasses)
      @table_original = {}
      @table_new = {}
      @table_old = {}
      @seq_name = {}
      klasses.each do |klass|
        @table_original[klass] = klass.table_name
        @table_new[klass] = table_name_new(klass)
        @table_old[klass] = table_name_old(klass)
        @seq_name[klass] = seq_name(klass)
      end
    end

    def self.set_tables_new(klasses)
      klasses.each do |klass|
        klass.table_name = @table_new[klass]
      end
    end

    def self.set_tables_original(klasses)
      klasses.each do |klass|
        klass.table_name = @table_original[klass]
      end
    end

    # Note: create table t2 (like t1) оставляет прявязку к тому же sequence, что и t1,
    #        то есть добавление строки в любую из них вызывает изменение значения sequence
    #        (поэтому приходится пересоздавать sequence для новых таблиц вручную)
    def self.create_tables_new(klasses)
      klasses.first.transaction do
        klasses.each do |klass|
          sql = <<EOS
            DROP TABLE IF EXISTS #{@table_new[klass]};
            DROP SEQUENCE IF EXISTS #{@seq_name[klass]} CASCADE;
            CREATE SEQUENCE #{@seq_name[klass]};
            CREATE TABLE #{@table_new[klass]} ( LIKE #{@table_original[klass]} INCLUDING DEFAULTS INCLUDING CONSTRAINTS INCLUDING INDEXES );
            ALTER TABLE #{@table_new[klass]} ALTER id SET DEFAULT nextval('#{@seq_name[klass]}');
EOS
          klass.connection.execute(sql)
        end
      end
    end

    def self.rotate_tables(klasses)
      klasses.first.transaction do
        klasses.each do |klass|
          sql = <<EOS
            DROP TABLE IF EXISTS #{@table_old[klass]};
            ALTER TABLE #{@table_original[klass]} RENAME TO #{@table_old[klass]};
            ALTER TABLE #{@table_new[klass]}      RENAME TO #{@table_original[klass]};
EOS
          klass.connection.execute(sql)
        end
      end
    end

  end
end
