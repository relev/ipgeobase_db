module IpgeobaseDb
  class Engine < ::Rails::Engine
    isolate_namespace IpgeobaseDb
    config.autoload_paths += %W(#{config.root}/lib)
  end
end
