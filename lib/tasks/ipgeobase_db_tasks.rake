namespace :ipgeobase_db do

  desc 'Import ipgeobase into db (import=download+load)'
  task import: %i(download load) do
    puts 'Import completed'
  end

  desc 'Download ipgeobase file'
  task download: %i(environment) do
    puts 'Downloading...'
    IpgeobaseDb::ImportService.download
  end


  desc 'Load from ipgeobase file'
  task load: %i(environment) do
    puts 'Loading...'
    IpgeobaseDb::ImportService.load
  end

end
