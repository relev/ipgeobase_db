class CreateDistricts < ActiveRecord::Migration
  def change
    create_table :ipgeobase_db_districts do |t|
      t.string :country_code, limit: 2, nil: false
      t.string :name, nil: false
      t.timestamp :created_at, nil: false
    end
  end
end
