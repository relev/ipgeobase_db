class CreateInetFunctions < ActiveRecord::Migration
  def change
    IpgeobaseDb::City.connection.execute("
      CREATE OR REPLACE FUNCTION inet_aton(INET)
        RETURNS BIGINT AS
          'SELECT INETMI($1,''0.0.0.0'') + (1<<31);'
        LANGUAGE SQL IMMUTABLE;
    ")

    IpgeobaseDb::City.connection.execute("
      CREATE OR REPLACE FUNCTION inet_ntoa(INT)
        RETURNS INET AS
          'SELECT ''0.0.0.0''::inet+($1-(1<<31));'
        LANGUAGE SQL IMMUTABLE;
    ")
  end
end
