class CreateCities < ActiveRecord::Migration
  def change
    create_table :ipgeobase_db_cities do |t|
      t.references :district, nil: false, index: true
      t.references :region, nil: false, index: true
      t.decimal :lat, precision: 9, scale: 6, nil: false
      t.decimal :lon, precision: 9, scale: 6, nil: false
      t.string :country_code, limit: 2, nil: false
      t.string :district_name, nil: false
      t.string :region_name, nil: false
      t.string :name, nil: false
      t.timestamp :created_at, nil: false
    end
  end
end
