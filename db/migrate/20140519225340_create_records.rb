class CreateRecords < ActiveRecord::Migration
  def change
    create_table :ipgeobase_db_records do |t|
      t.integer :low, nil: false
      t.integer :high, nil: false
      t.references :district, nil: false, index: true
      t.references :region, nil: false, index: true
      t.references :city, nil: false, index: true
      t.string :country_code, limit: 2, nil: false, index: true
      t.timestamp :created_at, nil: false
    end

    add_index :ipgeobase_db_records, %i(low high), unique: true
  end
end
