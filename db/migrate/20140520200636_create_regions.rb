class CreateRegions < ActiveRecord::Migration
  def change
    create_table :ipgeobase_db_regions do |t|
      t.references :district, nil: false
      t.string :country_code, limit: 2, nil: false
      t.string :district_name, nil: false
      t.string :name, nil: false
      t.timestamp :created_at, nil: false
    end
  end
end
