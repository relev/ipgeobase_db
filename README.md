# Это engine, не application!

* Загрузка осуществляется в новую временную таблицу, которая по окончании процесса замещает таблицу, используемую рельсами

# Usage:

* rake -T | grep ipgeobase\_db

* rake ipgeobase\_db:download - Скачивает файлы данных с http://ipgeobase.ru и сохраняет их в /tmp/ipgeobase\_db

* rake ipgeobase\_db:load - Распаковывает скаченное и загружает в базу

* rake ipgeobase\_db:import = download + load

* IpgeobaseDb::ImportService.download

* IpgeobaseDb::ImportService.load

* IpgeobaseDb::InfoService.record('83.69.106.68').city
